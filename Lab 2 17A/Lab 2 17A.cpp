

/* 
 * File:   Lab 2.cpp
 * Author: Heidi Dye
 *
 * Created on April 18, 2019, 7:42 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;
int* elementShifter(int*, int);
void one();
void two();
int wordCounter(string*);

int main(int argc, char** argv) {

    one();
    two();
    return 0;
}

void one()
{
       cout << "#1"<<endl<<endl;
    cout << "How big do you want your array to be?"<<endl;
    int size = 0;
    cin >> size;
    
    int* array = new int[size];
    for (int i=0; i<size; i++)
    {
        array[i]= rand()%100 +1;
    }
        cout<<"Before Shifting: ";
    for(int i=0; i<(size);i++)
    {
        cout<<array[i]<<" ";
    }
    cout<<endl;
    
    int* newarray = elementShifter(array, size);
    cout<<"Shifted Array: ";
    for(int i=0; i<(size+1);i++)
    {
        cout<<newarray[i]<<" ";
    }
    cout<<endl; 
}

int* elementShifter(int* oldArray, int size)
{
    size = size +1;
    int* newArray = new int[size];
    for (int i = 0; i<size; i++)
    {
        if(i == 0)
        {
            newArray[i]= 0;
        }
        else
        {
            newArray[i]= oldArray[i-1];
        }
    }
    return newArray;
}
void two()
{
    cout<<"#2"<<endl<<endl;
    
    
}

int wordCounter(string* userinput)
{
    
}